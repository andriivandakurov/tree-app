import { createContext } from 'react';

const TreeContext = createContext({
  isAllNodesExpanded: false,
  checkNodeState: () => { }
});

export default TreeContext;
