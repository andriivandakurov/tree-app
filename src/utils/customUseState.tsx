import React from 'react';
import ReactDOM from 'react-dom';
import App from '../App';

// This implementation is really primitive and it's working only for top level node.
const getNewState = () => {
  const _instance = {
    state: null,
    stateInitialized: false,
    component: null,
    setState(newState: any) {
      this.state = newState;
      ReactDOM.render(<App />, document.getElementById('root'));
    },
    customUseState(initialValue: any): any[] {
      if (!this.stateInitialized) {
        this.stateInitialized = true;
        this.state = initialValue;
      }
      return [this.state, this.setState.bind(this)];
    }
  }

  return _instance.customUseState.bind(_instance);
};

export const customUseState = getNewState();
