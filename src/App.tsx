import React from 'react';
import JsonTree from './components/JsonTree';
import TreeContext from './contexts/TreeContext';
import { customUseState } from './utils/customUseState';
import { data } from './dataMock';

const App: React.FC = () => {
  const [isAllNodesExpanded, toggleNodes] = customUseState(false);  
  const collapseBtnTitle = isAllNodesExpanded ? 'Collapse all' : 'Expand all';
  const checkNodeState = () => {
    // I know that this is primitive solution, but can't find any better solution in so short time frame.
    const openNodes = document.querySelectorAll('#root .open').length;
    const closedNodes = document.querySelectorAll('#root .close').length;

    // All nodes are expanded
    if (openNodes && !closedNodes) {
      toggleNodes(true);
    }

    // All nodes are collapsed
    if (!openNodes && closedNodes) {      
      toggleNodes(false);
    }
  }

  return (
    <TreeContext.Provider value={{ isAllNodesExpanded, checkNodeState }}>
      <div className="top-bar">
        <button type="button" onClick={() => toggleNodes(!isAllNodesExpanded)}>{collapseBtnTitle}</button>
      </div>

      <JsonTree isVisible={true} data={data} />
    </TreeContext.Provider>
  );
}

export default App;
