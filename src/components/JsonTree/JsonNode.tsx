import React, { FC, useState, useContext, useEffect } from 'react';
import JsonTree from './JsonTree';
import TreeContext from '../../contexts/TreeContext';
import { IJsonNodeProps } from './types';

const JsonNode: FC<IJsonNodeProps> = ({ data, title }) => {
  const {
    isAllNodesExpanded,
    checkNodeState
  } = useContext(TreeContext);
  const [isOpen, setOpenState] = useState(isAllNodesExpanded);
  const isComplexObject = typeof data === 'object';
  const component = isComplexObject
    ? <JsonTree data={data} isVisible={isOpen} />
    : data;

  // Here we can use `classnames` library ( https://github.com/JedWatson/classnames ), 
  // but as it's test project i decided to omit it.
  const nodeClassName = isComplexObject ? (isOpen ? 'open' : 'close') : '';

  useEffect(() => {
    setOpenState(isAllNodesExpanded);
  }, [isAllNodesExpanded]);

  useEffect(() => {
    checkNodeState();
  }, []);

  return (
    <li
      onClick={(e) => {
        e.stopPropagation();
        isComplexObject && setOpenState(!isOpen);
      }}
      className={nodeClassName}
    >
      "{title}": {component}
    </li>
  )
}

JsonNode.displayName = 'JsonNode';

export default JsonNode;
