import React, { FC } from 'react';
import JsonNode from './JsonNode';

import withCollapsedView from '../../hocs/withCollapsedView';
import withDelimiterByType from '../../hocs/withDelimiterByType';

import { IJsonTreeProps } from './types';
import './JsonTree.scss';

const JsonTree: FC<IJsonTreeProps> = ({ data }) => {
  return (
    <ul className="json-tree">
      {
        Object.keys(data).map((key: string, index: number) => {
          return <JsonNode data={data[key]} title={key} key={`${key}${index}`} />
        })
      }
    </ul>
  )
}

JsonTree.displayName = 'JsonTree';

export default withDelimiterByType(withCollapsedView(JsonTree));