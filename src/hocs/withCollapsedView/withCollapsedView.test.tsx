import React, { FC } from 'react';
import { render, cleanup } from '@testing-library/react';
import withCollapsedView from '.';
import { IWithCollapsedView } from './types';

describe('withCollapsedView hoc', () => {
  const Component: FC<any> = () => <h1>Original component</h1>;  
  const ComponentWithHOC: FC<IWithCollapsedView> = withCollapsedView(Component);

  afterEach(cleanup);

  it('should render default collapsed view', () => {
    const { container } = render(<ComponentWithHOC isVisible={false} />);

    expect(container.innerHTML).toBe('...');
  });

  it('should render expanded view', () => {
    const AnotherComponent = () => <div>Something</div>;
    const { container: anotherComponent } = render(<AnotherComponent />)
    const { container } = render(<ComponentWithHOC isVisible={true} />);
    const { container: originalComponent } = render(<Component />);

    expect(container).toEqual(originalComponent);
    expect(container).not.toEqual(anotherComponent);
  });

  it('should render provided collapsed view', () => {
    const collapsedText = 'collapsed';
    const { container } = render(<ComponentWithHOC isVisible={false} collapsedText={collapsedText} />);

    expect(container.innerHTML).toBe(collapsedText);
  });
});