export interface IWithCollapsedView {
  isVisible: boolean;
  collapsedText?: string;
}