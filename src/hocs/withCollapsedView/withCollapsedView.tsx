import React, { ComponentType, FC } from 'react';
import { IWithCollapsedView } from './types';

const withCollapsedView = <P extends object>(WrappedComponent: ComponentType<P>): FC<P & IWithCollapsedView> => ({
  isVisible,
  collapsedText = '...',
  ...props
}) => isVisible
    ? <WrappedComponent {...props as P} />
    : <>{collapsedText}</>

export default withCollapsedView;