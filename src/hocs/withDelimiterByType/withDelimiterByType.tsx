import React, { ComponentType, FC } from 'react';
import { IWithDelimiterByType } from './types';

const withDelimiterByType = <P extends object>(WrappedComponent: ComponentType<P>): FC<P & IWithDelimiterByType> => props => {
  const { data } = props;
  const isComplexObject = typeof data === 'object';

  if (!isComplexObject) {
    return (
      <WrappedComponent {...props} />
    )
  }

  const delimiter = Array.isArray(data) ? '[]' : '{}';
  const [before, after] = delimiter.split('');

  return (
    <>
      {before}
      <WrappedComponent {...props} />
      {after}
    </>
  )
}

export default withDelimiterByType;
